# 使用百度的定位

版本：
 
    android studio：2021.2.1
    
    jdk：1.8
    
    android：11
    
    百度Android 地图 SDK V7.5.2

百度提供的教程：[Android地图SDK | 百度地图API SDK (baidu.com)](https://lbsyun.baidu.com/index.php?title=androidsdk)

## 1，获取百度的key用于获取定位

 1. ### 申请成为百度的开发者

    地址：

    [控制台 | 百度地图开放平台 (baidu.com)](https://lbsyun.baidu.com/apiconsole/auth/result)

    教程：

    [百度地图开放平台的认证-百度经验 (baidu.com)](https://jingyan.baidu.com/article/36d6ed1f69c2b51bcf488388.html)

 2. ### 开始获取key

    地址：

    [控制台 | 百度地图开放平台 (baidu.com)](https://lbsyun.baidu.com/apiconsole/key#/home)

    教程：

    [androidsdk | 百度地图API SDK (baidu.com)](https://lbsyun.baidu.com/index.php?title=androidsdk/guide/create-project/ak)

    1. 获取对于debug 的SHA1 

       keytool -list -v -keystore debug.keystore  

       问题：

       在cmd中出现：'keytool' 不是内部或外部命令，也不是可运行的程序 或批处理文件

       解决：

       是找不到keytool工具，该工具在jdk的路径中 在jdk的bin目录中使用该命令，不知道怎么在c盘弄

       教程：

       [(11条消息) 'keytool' 不是内部或外部命令，也不是可运行的程序 或批处理文件。_www121104115的博客-CSDN博客_keytool' 不是内部或外部命令](https://blog.csdn.net/www121104115/article/details/79411110)

    2. 发布版本的SHA1 是在android studio中获取

       1. 获取应用的签名文件

          要通过jks文件才能获取发布的sha1 

          教程：

          [(11条消息) Android Studio生成.jks文件_m1m-FG的博客-CSDN博客_androidjks生成](https://blog.csdn.net/SakuraMG/article/details/126049757?spm=1001.2101.3001.6650.7&utm_medium=distribute.pc_relevant.none-task-blog-2~default~BlogCommendFromBaidu~Rate-7-126049757-blog-98494057.pc_relevant_aa&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2~default~BlogCommendFromBaidu~Rate-7-126049757-blog-98494057.pc_relevant_aa&utm_relevant_index=7)

       2. 在jdk的路径中 在jdk的bin目录中使用该命令

          keytool -list -v -keystore jks文件路径  

          同：获取对于debug 的SHA1 

 3. 获取到key

 4. 下载包

​	路径：[Android地图SDK | 百度地图API SDK (baidu.com)](https://lbsyun.baidu.com/index.php?title=androidsdk/sdkandev-download)

## 2，代码注意事项

1. ### 引入jar包

   在所有文件发在libs包下，BaiduLBS_Android.jar需要 Add As Library...

![输入图片说明](https://foruda.gitee.com/images/1661483681056743604/eb51ca6b_3048388.jpeg "微信图片_20220826105003.jpg")
2. ### 在build.gradle的文件中添加

   dependencies{}中添加

   ```
   implementation files('libs\\BaiduLBS_Android.jar')
   ```

   路径和BaiduLBS_Android.jar的位置要匹配

   在android {}中添加

   ```
   sourceSets{
       main{
   	    jniLibs.srcDirs = ['libs']
       }
   }
   ```

3. ###  AndroidManifest.xml

   添加

   ```
   <meta-data
       android:name="com.baidu.lbsapi.API_KEY"
       android:value="百度的key"/>
   ```

   在activity的节点下添加

   ```
   <service android:name="com.baidu.location.f" android:enabled="true" android:process=":remote"> </service>
   ```

   添加权限

   ```
   <!-- 这个权限用于进行网络定位-->
   <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"></uses-permission>
   <!-- 这个权限用于访问GPS定位-->
   <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"></uses-permission>
   <!-- 用于访问wifi网络信息，wifi信息会用于进行网络定位-->
   <uses-permission android:name="android.permission.ACCESS_WIFI_STATE"></uses-permission>
   <!-- 获取运营商信息，用于支持提供运营商信息相关的接口-->
   <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"></uses-permission>
   <!-- 这个权限用于获取wifi的获取权限，wifi信息会用来进行网络定位-->
   <uses-permission android:name="android.permission.CHANGE_WIFI_STATE"></uses-permission>
   <!-- 用于读取手机当前的状态-->
   <uses-permission android:name="android.permission.READ_PHONE_STATE"></uses-permission>
   <!-- 写入扩展存储，向扩展卡写入数据，用于写入离线定位数据-->
   <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"></uses-permission>
   <!-- 访问网络，网络定位需要上网-->
   <uses-permission android:name="android.permission.INTERNET" />
   <!-- SD卡读取权限，用户写入离线定位数据-->
   <uses-permission android:name="android.permission.MOUNT_UNMOUNT_FILESYSTEMS"
       tools:ignore="ProtectedPermissions"></uses-permission>
   ```
![输入图片说明](https://foruda.gitee.com/images/1661483644111774922/0e16b3cf_3048388.jpeg "QQ截图20220826110000.jpg")



## 3，代码位置

https://gitee.com/kai2019/location.git